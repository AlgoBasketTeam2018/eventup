<?php
class Auth extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('Auth_model');
  }

  function login(){
    $this->load->library('form_validation');
    if($this->input->post('submit')){
      $this->form_validation->set_rules('required|is_email|max_length[30]|alpha_numeric');
      $this->form_validation->set_rules('required|max_length[30]|alpha_numeric');
      if($thia->form_validation->run() == FALSE){
         $this->validation_errors();
      }else{
         $usernameOrEmail = $this->input->post('username');
         $password = $this->input->post('password');
         if($this->auth->isLoginValid($usernameOrEmail,md5($password)) == TRUE){
            $userData = $this->auth->getLoginDetail($usernameOrEmail,md5($password));
              $this->session->set_userdata(array(
                'userid'   => $userData['user_id'],
                'username' => $userData['username'],
                'email'    => $userData['email'],
                'status'   => $userData['status'],
                'rolename' => $userData['rolename']
              ));
              redirect('welcome');
         }else{
           $this->session->set_flashdata('alert',alert('loginError'));
         }
      } // END-ELSE
    } // END-IF
    $this->load->view('template/content',['page' => 'auth']);
  }

  function register(){
     $this->load->view('template/content',['page' => 'auth']);
  }


}
 ?>
