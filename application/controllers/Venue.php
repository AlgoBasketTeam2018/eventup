<?php
 class Venue extends CI_Controller{

   function __construct(){
     parent::__construct();
   }

   function index(){
      $this->load->view('template/content',['page' => 'venue']);
   }

   function detail(){
     $this->load->view('template/content',['page' => 'venue-detail']);
   }

 }
?>
