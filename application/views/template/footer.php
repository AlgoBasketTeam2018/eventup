<footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
     <p class="href-normal">
      <a href="">BLOG</a>
      <a href="">CONTACT US</a>
      <a href="">PRESS</a>
      <a href="">PRIVACY POLICY</a>
      <a href="">TERMS OF SERVICE</a>
     </p>
     <p>HawaiiVenues is &copy; Algobasket's product, all right reserved to the owner!</p>
    <p>New to Hawaii Venues? <a href="<?php echo base_url();?>">Visit the homepage</a> or read our <a href="<?php echo base_url();?>About-Us">getting started guide</a>.</p>
  </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>
<script src="<?php echo base_url();?>assets/js/script.js"></script>
</body>
</html>
