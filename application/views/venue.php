<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("budget");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>
<div class="container-fluid">
<br><br>
 <h3>Event venues in Hawaiian Gardens, CA</h3>
  <div class="row">
  <div class="col-md-3">
    <ul class="list-group">
     <li class="list-group-item"><h4>Filters</h4></li>
     <li class="list-group-item">Price Range</li>
     <li class="list-group-item">
       <button class="btn btn-dark">$</button>
       <button class="btn btn-dark">$$</button>
       <button class="btn btn-dark">$$$</button>
       <button class="btn btn-dark">$$$$</button>
     </li>
     <li class="list-group-item">BUDGET</li>
     <li class="list-group-item">
       <div class="slidecontainer">
         <input type="range" min="1" max="100" value="50" class="slider" id="myRange">
         <p>Value: <span id="budget"></span></p>
       </div>
     </li>
     <li class="list-group-item">NUMBER OF ATTENDEES</li>
     <li class="list-group-item">
       Seated <input type="radio" />
       Standing<input type="radio" />
       <div class="slidecontainer">
         <input type="range" min="1" max="100" value="50" class="slider" id="myRange">
         <p>Value: <span id="budget"></span></p>
       </div>
     </li>
     <li class="list-group-item">NEIGHBORHOODS</li>
     <li class="list-group-item">
       <div class="col-md-5"></div>

        <input type="radio" /> Seated<br>
        <input type="radio" /> Standing<br>
     </li>
     <li class="list-group-item">AMENITIES</li>
     <li class="list-group-item">
       <div class="col-md-5"></div>

        <input type="radio" /> Seated<br>
        <input type="radio" /> Standing<br>
     </li>
     <li class="list-group-item">VENUE TYPE</li>
     <li class="list-group-item">
       <div class="col-md-5"></div>

        <input type="radio" /> Seated<br>
        <input type="radio" /> Standing<br>
     </li>
     <li class="list-group-item">LOOK & FEEL</li>
     <li class="list-group-item">
       <div class="col-md-5"></div>

        <input type="radio" /> Seated<br>
        <input type="radio" /> Standing<br>
     </li>
     <li class="list-group-item">
       <button type="submit" class="btn btn-primary btn-sm">Reset Filter</button>
     </li>
    </ul>
    <br><br>
  </div>
  <div class="col-md-9">

    <div class="col-md-4 float-left placeThumbnail" data-target="<?php echo base_url();?>">
      <div class="card mb-4 box-shadow">
        <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=MOLOKA'I" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">
          MOLOKA'I
          </p>
          <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
             Occupancy : 160
            </div>
            <small class="text-muted">9 mins</small>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 float-left placeThumbnail" data-target="<?php echo base_url();?>">
      <div class="card mb-4 box-shadow">
        <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=MOLOKA'I" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">
          MOLOKA'I
          </p>
          <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
             Occupancy : 160
            </div>
            <small class="text-muted">9 mins</small>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 float-left placeThumbnail" data-target="<?php echo base_url();?>">
      <div class="card mb-4 box-shadow">
        <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=MOLOKA'I" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">
          MOLOKA'I
          </p>
          <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
             Occupancy : 160
            </div>
            <small class="text-muted">9 mins</small>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 float-left placeThumbnail" data-target="<?php echo base_url();?>">
      <div class="card mb-4 box-shadow">
        <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=MOLOKA'I" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">
          MOLOKA'I
          </p>
          <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
             Occupancy : 160
            </div>
            <small class="text-muted">9 mins</small>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 float-left">
      <div class="card mb-4 box-shadow">
        <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text= + LOAD MORE" alt="Card image cap">
      </div>
    </div>






  </div>
</div>

</div>
