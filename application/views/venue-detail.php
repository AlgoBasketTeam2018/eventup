
<div class="container-fluid">
  <br>
  <nav aria-label="breadcrumb">
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="#">Home</a></li>
     <li class="breadcrumb-item"><a href="#">North Carolina</a></li>
     <li class="breadcrumb-item active" aria-current="page">Cary</li>
   </ol>
 </nav>
 <div class="jumbotron jumbotron-fluid bgImageJumbotron2">
 <div class="container">
   <h1 class="display-4">Fluid jumbotron</h1>
   <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
 </div>
</div>
<br>
  <div class="row">
  <div class="col-md-9">
    <ul class="list-unstyled">
      <li class="media">
         <h5 class="mt-0 mb-1" style="width:200px"></h5>
        <div class="media-body">
          <h5 class="mt-0 mb-1">Citi Field</h5>
          <h6><b>Seated: 3420  | Standing: 6000</b></h6>
          <h6>
            FRI - SAT: Quote Request For Info<br>
            FRI - SAT BUYOUT: Quote Request For Info<br>
            SUN - THURS: $5,000<br>
            SUN - THURS BUYOUT: Quote Request For Info<br>
            WEDDINGS: Quote Request For Info<br>
          </h6>
        </div>
      </li>
    <li class="media">
       <h5 class="mt-0 mb-1" style="width:200px">PRICING</h5>
      <div class="media-body">
        <h5 class="mt-0 mb-1">Citi Field</h5>
        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
      </div>
    </li>
    <li class="media my-4">
      <h5 class="mt-0 mb-1" style="width:200px">CLOSER LOOK</h5>
      <div class="media-body">
        
          <a href="" class="btn btn-dark">REVIEWS</a>
          <a href="" class="btn btn-dark">VIDEOS</a>
          <a href="" class="btn btn-dark">FAQ</a>
          <a href="" class="btn btn-dark">FLOOR PLANS</a>
      </div>
    </li>
    <li class="media">
      <h5 class="mt-0 mb-1" style="width:200px">LOCATION</h5>
      <div class="media-body">
        <h5 class="mt-0 mb-1">LOCATION</h5>
        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
      </div>
    </li>
</ul>
  </div>
  <div class="col-md-3">
    <br><br>
     <div class="well">
     <div class="d-inline p-2 bg-primary text-white">d-inline</div>
     <div class="d-inline p-2 bg-dark text-white">d-inline</div>
     </div>
     <div class="well">
      <select class="form-control">
       <option>Venue Type</option>
      </select>
     </div>
  </div>
  </div>

</div>
