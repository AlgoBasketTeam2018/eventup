
    <main role="main">

      <section class="jumbotron text-center bgImageJumbotron image-container">
        <div class="overlay"></div>
        <div class="container">
          <h1 class="heading-banner">FIND YOUR PERFECT HAWAII</h1>
          <p class="lead text-muted" style="color:#ddd">Browse venues in over hawaii island</p>
          <p>
            <form action="/venue">
              <div class="row">
                 <div class="col-md-10">
                  <input type="text" class="form-control" placeholder="Search">
                  <div style="position:absolute;width:94%;background:white;height:100px;margin:5px;display:none">
                   <ul class="list-group" style="text-align:left">
                      <li class="list-group-item"><b>Cities and Neighbor hoods</b></li>
                      <li class="list-group-item">Hawaiian Garden</li>
                      <li class="list-group-item">Hawaiian Volcano National Park</li>
                      <li class="list-group-item">Honolulu Garden</li>
                      <li class="list-group-item"><b>Venues</b></li>
                      <li class="list-group-item">Hawaii Island Retreat</li>
                   </ul>
                  </div>
                 </div>
                 <div class="col-md-2">
                   <button type="submit" class="btn btn-primary mb-2">SEARCH</button>
                 </div>


              </div>

            </form>
          </p>
        </div>
      </section>
       <center>
       <img src="./assets/img/header-slide.png" />
       </center>
       <br>
       <center><h3>Become A Venue Partner</h3></center>
       <center><p><small>Partner with Rental and showcase your property to a national audience. We deliver <br>
         corporate clients, private dining experiences, and all other social events..</small></p></center>
      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=MOLOKA'I" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">
                  MOLOKA'I
                  </p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     Occupancy : 160
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=MAUI" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">
                  MAUI
                  </p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     Occupancy : 160
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=KAHO'OLAWE" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">
                  KAHO'OLAWE
                  </p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     Occupancy : 160
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>


          </div>
          <center ><button type="button" class="btn btn-outline-secondary"><small>LIST YOUR LUXURY PROPERTY SPACE</small></button></center>
        </div>
      </div>

      <div class="container">
       <h2 class="text-center"><img src="./assets/img/palm.jpg" width="80"/>List Your Resort Today</h3>
        <center>
          <img src="./assets/img/list-resort.png" width="99%" style="border-radius:14px"/>
        </center>
     </div>
    </main>
    <br>
    <div class="container">
       <div class="jumbotron mt-3 bgImageJumbotron2" >
         <h1 style="color:#FFF">Download Our Mobile App app</h1>
         <p class="lead" style="color:#FFF">
           Browse and book your luxury resorts anytime
         </p>
          <h4 style="color:#FFF">Take your vacation with us</h4>
         <a href="../../components/navbar/" role="button"><img src="./assets/img/playicon.png" width="150px" /></a>
         <a href="../../components/navbar/" role="button"><img src="./assets/img/apple.png" width="170px"/></a>
       </div>
     </div>
     <br><br>
